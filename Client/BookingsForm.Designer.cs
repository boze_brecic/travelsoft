﻿namespace Client
{
    partial class BookingsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.addNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.deleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnShowAll = new System.Windows.Forms.Button();
            this.dataBookingsView = new System.Windows.Forms.DataGridView();
            this.btnPopulateAdr = new System.Windows.Forms.Button();
            this.lblRoomNum = new System.Windows.Forms.Label();
            this.cmbAddress = new System.Windows.Forms.ComboBox();
            this.BtnAction = new System.Windows.Forms.Button();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txbId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.cmbRoomNum = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.cmbGuest = new System.Windows.Forms.ComboBox();
            this.BtnPopulateGuests = new System.Windows.Forms.Button();
            this.dateTimeCheckIn = new System.Windows.Forms.DateTimePicker();
            this.dateTimeCheckOut = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBookingsView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addNewToolStripMenuItem,
            this.editToolStripMenuItem,
            this.deleteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1136, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // addNewToolStripMenuItem
            // 
            this.addNewToolStripMenuItem.Name = "addNewToolStripMenuItem";
            this.addNewToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.addNewToolStripMenuItem.Text = "Add New";
            this.addNewToolStripMenuItem.Click += new System.EventHandler(this.addNewToolStripMenuItem_Click);
            // 
            // editToolStripMenuItem
            // 
            this.editToolStripMenuItem.Name = "editToolStripMenuItem";
            this.editToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.editToolStripMenuItem.Text = "Edit";
            this.editToolStripMenuItem.Click += new System.EventHandler(this.editToolStripMenuItem_Click);
            // 
            // deleteToolStripMenuItem
            // 
            this.deleteToolStripMenuItem.Name = "deleteToolStripMenuItem";
            this.deleteToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.deleteToolStripMenuItem.Text = "Delete";
            this.deleteToolStripMenuItem.Click += new System.EventHandler(this.deleteToolStripMenuItem_Click);
            // 
            // BtnShowAll
            // 
            this.BtnShowAll.Location = new System.Drawing.Point(367, 37);
            this.BtnShowAll.Name = "BtnShowAll";
            this.BtnShowAll.Size = new System.Drawing.Size(161, 55);
            this.BtnShowAll.TabIndex = 4;
            this.BtnShowAll.Text = "Show All";
            this.BtnShowAll.UseVisualStyleBackColor = true;
            this.BtnShowAll.Click += new System.EventHandler(this.BtnShowAll_Click);
            // 
            // dataBookingsView
            // 
            this.dataBookingsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataBookingsView.Location = new System.Drawing.Point(12, 98);
            this.dataBookingsView.Name = "dataBookingsView";
            this.dataBookingsView.RowTemplate.Height = 24;
            this.dataBookingsView.Size = new System.Drawing.Size(957, 302);
            this.dataBookingsView.TabIndex = 3;
            // 
            // btnPopulateAdr
            // 
            this.btnPopulateAdr.Location = new System.Drawing.Point(442, 526);
            this.btnPopulateAdr.Name = "btnPopulateAdr";
            this.btnPopulateAdr.Size = new System.Drawing.Size(87, 47);
            this.btnPopulateAdr.TabIndex = 30;
            this.btnPopulateAdr.Text = "Populate Addresses";
            this.btnPopulateAdr.UseVisualStyleBackColor = true;
            this.btnPopulateAdr.Click += new System.EventHandler(this.btnPopulateAdr_Click);
            // 
            // lblRoomNum
            // 
            this.lblRoomNum.AutoSize = true;
            this.lblRoomNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomNum.Location = new System.Drawing.Point(39, 593);
            this.lblRoomNum.Name = "lblRoomNum";
            this.lblRoomNum.Padding = new System.Windows.Forms.Padding(4);
            this.lblRoomNum.Size = new System.Drawing.Size(156, 33);
            this.lblRoomNum.TabIndex = 28;
            this.lblRoomNum.Text = "Room Number";
            // 
            // cmbAddress
            // 
            this.cmbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAddress.FormattingEnabled = true;
            this.cmbAddress.Location = new System.Drawing.Point(201, 535);
            this.cmbAddress.Name = "cmbAddress";
            this.cmbAddress.Size = new System.Drawing.Size(235, 28);
            this.cmbAddress.TabIndex = 27;
            this.cmbAddress.Text = "Select available address";
            this.cmbAddress.SelectedIndexChanged += new System.EventHandler(this.cmbAddress_SelectedIndexChanged);
            // 
            // BtnAction
            // 
            this.BtnAction.Location = new System.Drawing.Point(724, 570);
            this.BtnAction.Name = "BtnAction";
            this.BtnAction.Size = new System.Drawing.Size(132, 56);
            this.BtnAction.TabIndex = 26;
            this.BtnAction.UseVisualStyleBackColor = true;
            this.BtnAction.Click += new System.EventHandler(this.BtnAction_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(39, 530);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Padding = new System.Windows.Forms.Padding(4);
            this.lblAddress.Size = new System.Drawing.Size(100, 33);
            this.lblAddress.TabIndex = 25;
            this.lblAddress.Text = "Address";
            // 
            // txbId
            // 
            this.txbId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbId.Location = new System.Drawing.Point(201, 421);
            this.txbId.Name = "txbId";
            this.txbId.Size = new System.Drawing.Size(235, 26);
            this.txbId.TabIndex = 24;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(39, 414);
            this.lblId.Name = "lblId";
            this.lblId.Padding = new System.Windows.Forms.Padding(4);
            this.lblId.Size = new System.Drawing.Size(38, 33);
            this.lblId.TabIndex = 23;
            this.lblId.Text = "Id";
            // 
            // cmbRoomNum
            // 
            this.cmbRoomNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbRoomNum.FormattingEnabled = true;
            this.cmbRoomNum.Location = new System.Drawing.Point(201, 593);
            this.cmbRoomNum.Name = "cmbRoomNum";
            this.cmbRoomNum.Size = new System.Drawing.Size(235, 28);
            this.cmbRoomNum.TabIndex = 31;
            this.cmbRoomNum.Text = "Select available room";
            this.cmbRoomNum.SelectedIndexChanged += new System.EventHandler(this.cmbRoomNum_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(39, 473);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(4);
            this.label1.Size = new System.Drawing.Size(77, 33);
            this.label1.TabIndex = 32;
            this.label1.Text = "Guest";
            // 
            // cmbGuest
            // 
            this.cmbGuest.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbGuest.FormattingEnabled = true;
            this.cmbGuest.Location = new System.Drawing.Point(201, 478);
            this.cmbGuest.Name = "cmbGuest";
            this.cmbGuest.Size = new System.Drawing.Size(235, 28);
            this.cmbGuest.TabIndex = 33;
            this.cmbGuest.Text = "Select available guest";
            // 
            // BtnPopulateGuests
            // 
            this.BtnPopulateGuests.Location = new System.Drawing.Point(442, 469);
            this.BtnPopulateGuests.Name = "BtnPopulateGuests";
            this.BtnPopulateGuests.Size = new System.Drawing.Size(87, 47);
            this.BtnPopulateGuests.TabIndex = 34;
            this.BtnPopulateGuests.Text = "Populate Guests";
            this.BtnPopulateGuests.UseVisualStyleBackColor = true;
            this.BtnPopulateGuests.Click += new System.EventHandler(this.BtnPopulateGuests_Click);
            // 
            // dateTimeCheckIn
            // 
            this.dateTimeCheckIn.CustomFormat = "yyyy-MM-dd";
            this.dateTimeCheckIn.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeCheckIn.Location = new System.Drawing.Point(724, 440);
            this.dateTimeCheckIn.Name = "dateTimeCheckIn";
            this.dateTimeCheckIn.Size = new System.Drawing.Size(166, 22);
            this.dateTimeCheckIn.TabIndex = 35;
            // 
            // dateTimeCheckOut
            // 
            this.dateTimeCheckOut.CustomFormat = "yyyy-MM-dd";
            this.dateTimeCheckOut.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dateTimeCheckOut.Location = new System.Drawing.Point(724, 494);
            this.dateTimeCheckOut.Name = "dateTimeCheckOut";
            this.dateTimeCheckOut.Size = new System.Drawing.Size(166, 22);
            this.dateTimeCheckOut.TabIndex = 36;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(586, 429);
            this.label2.Name = "label2";
            this.label2.Padding = new System.Windows.Forms.Padding(4);
            this.label2.Size = new System.Drawing.Size(100, 33);
            this.label2.TabIndex = 37;
            this.label2.Text = "CheckIn";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(586, 483);
            this.label3.Name = "label3";
            this.label3.Padding = new System.Windows.Forms.Padding(4);
            this.label3.Size = new System.Drawing.Size(117, 33);
            this.label3.TabIndex = 38;
            this.label3.Text = "CheckOut";
            // 
            // BookingsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1136, 716);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.dateTimeCheckOut);
            this.Controls.Add(this.dateTimeCheckIn);
            this.Controls.Add(this.BtnPopulateGuests);
            this.Controls.Add(this.cmbGuest);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmbRoomNum);
            this.Controls.Add(this.btnPopulateAdr);
            this.Controls.Add(this.lblRoomNum);
            this.Controls.Add(this.cmbAddress);
            this.Controls.Add(this.BtnAction);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.txbId);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.BtnShowAll);
            this.Controls.Add(this.dataBookingsView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "BookingsForm";
            this.Text = "BookingsForm";
            this.Load += new System.EventHandler(this.BookingsForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataBookingsView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem addNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem deleteToolStripMenuItem;
        private System.Windows.Forms.Button BtnShowAll;
        private System.Windows.Forms.DataGridView dataBookingsView;
        private System.Windows.Forms.Button btnPopulateAdr;
        private System.Windows.Forms.Label lblRoomNum;
        private System.Windows.Forms.ComboBox cmbAddress;
        private System.Windows.Forms.Button BtnAction;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txbId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.ComboBox cmbRoomNum;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ComboBox cmbGuest;
        private System.Windows.Forms.Button BtnPopulateGuests;
        private System.Windows.Forms.DateTimePicker dateTimeCheckIn;
        private System.Windows.Forms.DateTimePicker dateTimeCheckOut;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}