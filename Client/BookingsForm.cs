﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class BookingsForm : Form
    {
        public BookingsForm()
        {
            InitializeComponent();
        }
        public void setAll(bool flag)
        {
            txbId.Enabled = flag;
            cmbAddress.Enabled = flag;
            BtnAction.Enabled = flag;
            cmbRoomNum.Enabled = flag;
            cmbGuest.Enabled = flag;
            btnPopulateAdr.Enabled = flag;
            BtnPopulateGuests.Enabled = flag;
            dateTimeCheckIn.Enabled = flag;
            dateTimeCheckOut.Enabled = flag;
        }

        public void UI(string action)
        {
            switch(action)
            {
                case "EDIT":
                    {
                        setAll(true);
                        BtnAction.Text = action;
                    }break;
                case "ADD NEW":
                    {
                        setAll(true);
                        txbId.Enabled = false;
                        BtnAction.Text = action;
                    }break;
                case "DELETE":
                    {
                        setAll(false);
                        txbId.Enabled = true;
                        BtnAction.Enabled = true;
                        BtnAction.Text = action;
                    }break;
            }
        }

        private void BtnShowAll_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/bookings").Result;
            var data = response.Content.ReadAsAsync<IEnumerable<Booking>>().Result;
            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Successfully loaded all data!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Something went wrong", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dataBookingsView.DataSource = data;
        }

        private void BookingsForm_Load(object sender, EventArgs e)
        {
            setAll(false);
            BtnAction.Text = "";
        }

        private void addNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("ADD NEW");
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("EDIT");
        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("DELETE");
        }

        private void btnPopulateAdr_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/buildings").Result;
            var dataAddress = response.Content.ReadAsAsync<IEnumerable<Building>>().Result;
            for (int i = 0; i < dataAddress.Count(); i++)
            {
                cmbAddress.Items.Add(dataAddress.ElementAt(i).Address.ToString());
            }
        }

        private void BtnPopulateGuests_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/guests").Result;
            var dataGuest = response.Content.ReadAsAsync<IEnumerable<Guest>>().Result;
            for (int i = 0; i < dataGuest.Count(); i++)
            {
                cmbGuest.Items.Add(dataGuest.ElementAt(i).FirstName.ToString() + " " + dataGuest.ElementAt(i).LastName.ToString());
            }
        }

        private void cmbRoomNum_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cmbAddress_SelectedIndexChanged(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/rooms").Result;
            var dataRoom = response.Content.ReadAsAsync<IEnumerable<Room>>().Result;
            cmbRoomNum.Items.Clear();
            for (int i = 0; i < dataRoom.Count(); i++)
            {
                if (dataRoom.ElementAt(i).Address == cmbAddress.Text)
                    cmbRoomNum.Items.Add(dataRoom.ElementAt(i).RoomNumber.ToString());
            }
        }

        private void BtnAction_Click(object sender, EventArgs e)
        {
            switch (BtnAction.Text)
            {
                case "EDIT":
                    {
                        try
                        {
                            if (txbId.Text == "" || cmbGuest.Text == "Select available guest" || cmbAddress.Text== "Select available address" || cmbRoomNum.Text== "Select available room" || dateTimeCheckIn.Text == dateTimeCheckOut.Text)
                                throw new Exception("You have not filled in some fields!");
                            Booking newBooking = new Booking() { Id = int.Parse(txbId.Text), FirstName = cmbGuest.Text.Split(' ')[0], RoomNumber = int.Parse(cmbRoomNum.Text), BuildingAddress = cmbAddress.Text, CheckIn = DateTime.Parse(dateTimeCheckIn.Text), CheckOut = DateTime.Parse(dateTimeCheckOut.Text) };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PutAsJsonAsync($"/api/bookings/{int.Parse(txbId.Text)}", newBooking).Result;
                        }
                        catch(Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "DELETE":
                    {
                        try
                        {
                            if (txbId.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.DeleteAsync($"/api/bookings/{int.Parse(txbId.Text)}").Result;
                        }
                        catch(Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "ADD NEW":
                    {
                        try
                        {
                            if (cmbGuest.Text == "Select available guest" || cmbAddress.Text == "Select available address" || cmbRoomNum.Text == "Select available room" || dateTimeCheckIn.Text == dateTimeCheckOut.Text)
                                throw new Exception("You have not filled in some fields!");
                            Booking newBooking = new Booking() { FirstName = cmbGuest.Text.Split(' ')[0], LastName = cmbGuest.Text.Split(' ')[1], RoomNumber = int.Parse(cmbRoomNum.Text), BuildingAddress = cmbAddress.Text, CheckIn = DateTime.Parse(dateTimeCheckIn.Text), CheckOut = DateTime.Parse(dateTimeCheckOut.Text) };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PostAsJsonAsync("/api/bookings/", newBooking).Result;
                        }
                        catch(Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();

                    }
                    break;

            }
        }
    }
}
