﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class BuildingsForm : Form
    {
        public BuildingsForm()
        {
            InitializeComponent();
        }
        public void setAll(bool flag)
        {
            txbId.Enabled = flag;
            BtnAction.Enabled = flag;
            txbAddress.Enabled = flag;

        }

        public void UI(string action)
        {
            switch (action)
            {
                case "EDIT":
                    {
                        setAll(true);
                        BtnAction.Text = action;
                    }
                    break;
                case "ADD NEW":
                    {
                        setAll(true);
                        txbId.Enabled = false;
                        BtnAction.Text = action;
                    }
                    break;
                case "DELETE":
                    {
                        setAll(true);
                        txbAddress.Enabled = false;
                        BtnAction.Text = action;
                    }
                    break;
            }
        }

        private void BuildingsForm_Load(object sender, EventArgs e)
        {
            setAll(false);
        }

        private void AddNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("ADD NEW");
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("EDIT");
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("DELETE");
        }

        private void BtnShowAll_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/buildings").Result;
            var data = response.Content.ReadAsAsync<IEnumerable<Building>>().Result;
            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Successfully loaded all data!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Something went wrong", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dataBuildingsView.DataSource = data;
        }

        private void BtnAction_Click(object sender, EventArgs e)
        {

            switch (BtnAction.Text)
            {
                case "EDIT":
                    {
                        try
                        {
                            if (txbId.Text == "" || txbAddress.Text=="")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            Building newBuiding = new Building() { Id = int.Parse(txbId.Text), Address = txbAddress.Text };
                            HttpResponseMessage response = client.PutAsJsonAsync($"/api/buildings/{int.Parse(txbId.Text)}", newBuiding).Result;
                        }
                        catch(Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "DELETE":
                    {
                        try
                        {
                            if (txbId.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.DeleteAsync($"/api/buildings/{int.Parse(txbId.Text)}").Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "ADD NEW":
                    {
                        try
                        {
                            if (txbAddress.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            Building newBuilding = new Building() { Address = txbAddress.Text };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PostAsJsonAsync("/api/buildings/", newBuilding).Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();

                    }
                    break;

            }
        }
    }
}
