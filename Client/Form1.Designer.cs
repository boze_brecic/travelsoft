﻿namespace Client
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStripGuests = new System.Windows.Forms.MenuStrip();
            this.GuestToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BuildingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.RoomsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BookingsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.label1 = new System.Windows.Forms.Label();
            this.menuStripGuests.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStripGuests
            // 
            this.menuStripGuests.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStripGuests.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.GuestToolStripMenuItem,
            this.BuildingsToolStripMenuItem,
            this.RoomsToolStripMenuItem,
            this.BookingsToolStripMenuItem});
            this.menuStripGuests.Location = new System.Drawing.Point(0, 0);
            this.menuStripGuests.Name = "menuStripGuests";
            this.menuStripGuests.Size = new System.Drawing.Size(963, 40);
            this.menuStripGuests.TabIndex = 0;
            this.menuStripGuests.Text = "menuStrip1";
            // 
            // GuestToolStripMenuItem
            // 
            this.GuestToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.GuestToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.GuestToolStripMenuItem.Name = "GuestToolStripMenuItem";
            this.GuestToolStripMenuItem.Padding = new System.Windows.Forms.Padding(8, 6, 8, 6);
            this.GuestToolStripMenuItem.Size = new System.Drawing.Size(81, 36);
            this.GuestToolStripMenuItem.Text = "GUESTS";
            this.GuestToolStripMenuItem.Click += new System.EventHandler(this.guestToolStripMenuItem_Click);
            // 
            // BuildingsToolStripMenuItem
            // 
            this.BuildingsToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BuildingsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BuildingsToolStripMenuItem.Name = "BuildingsToolStripMenuItem";
            this.BuildingsToolStripMenuItem.Size = new System.Drawing.Size(95, 36);
            this.BuildingsToolStripMenuItem.Text = "BUILDINGS";
            this.BuildingsToolStripMenuItem.Click += new System.EventHandler(this.BuildingsToolStripMenuItem_Click);
            // 
            // RoomsToolStripMenuItem
            // 
            this.RoomsToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.RoomsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.RoomsToolStripMenuItem.Name = "RoomsToolStripMenuItem";
            this.RoomsToolStripMenuItem.Size = new System.Drawing.Size(73, 36);
            this.RoomsToolStripMenuItem.Text = "ROOMS";
            this.RoomsToolStripMenuItem.Click += new System.EventHandler(this.RoomsToolStripMenuItem_Click);
            // 
            // BookingsToolStripMenuItem
            // 
            this.BookingsToolStripMenuItem.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.BookingsToolStripMenuItem.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.BookingsToolStripMenuItem.Name = "BookingsToolStripMenuItem";
            this.BookingsToolStripMenuItem.Size = new System.Drawing.Size(86, 36);
            this.BookingsToolStripMenuItem.Text = "BOOKING";
            this.BookingsToolStripMenuItem.Click += new System.EventHandler(this.BookingsToolStripMenuItem_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(323, 157);
            this.label1.Name = "label1";
            this.label1.Padding = new System.Windows.Forms.Padding(50);
            this.label1.Size = new System.Drawing.Size(223, 129);
            this.label1.TabIndex = 1;
            this.label1.Text = "TravelSoft";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 471);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.menuStripGuests);
            this.Name = "Form1";
            this.Text = "Form1";
            this.menuStripGuests.ResumeLayout(false);
            this.menuStripGuests.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStripGuests;
        private System.Windows.Forms.ToolStripMenuItem GuestToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BuildingsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem RoomsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BookingsToolStripMenuItem;
        private System.Windows.Forms.Label label1;
    }
}

