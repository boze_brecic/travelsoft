﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class GuestsForm : Form
    {
        public GuestsForm()
        {
            InitializeComponent();
        }
        public void setAll(bool flag)
        {
            txbId.Enabled = flag;
            txbFirstName.Enabled = flag;
            txbLastName.Enabled = flag;
            btnAction.Enabled = flag;

        }

        public void UI(string action)
        {
            switch (action)
            {
                case "EDIT":
                    {
                        setAll(true);
                        btnAction.Text = action;
                    }
                    break;
                case "ADD NEW":
                    {
                        setAll(true);
                        txbId.Enabled = false;
                        btnAction.Text = action;
                    }
                    break;
                case "DELETE":
                    {
                        setAll(false);
                        txbId.Enabled = true;
                        btnAction.Enabled = true;
                        btnAction.Text = action;
                    }
                    break;
            }
        }

        private void GuestsForm_Load(object sender, EventArgs e)
        {
            setAll(false);

        }

        private void addNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("ADD NEW");
        }

        private void editToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("EDIT");

        }

        private void deleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("DELETE");
        }

        private void btnAction_Click(object sender, EventArgs e)
        {
            switch(btnAction.Text)
            {
                case "EDIT":
                    {
                        try
                        {
                            if (txbId.Text == "" || txbFirstName.Text == "" || txbLastName.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            Guest newGuest = new Guest() { Id = int.Parse(txbId.Text), FirstName = txbFirstName.Text, LastName = txbLastName.Text };
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PutAsJsonAsync($"/api/guests/{int.Parse(txbId.Text)}", newGuest).Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        btnRefresh.PerformClick();
                    }
                    break;
                case "DELETE":
                    {
                        try
                        {
                            if (txbId.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.DeleteAsync($"/api/guests/{int.Parse(txbId.Text)}").Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        btnRefresh.PerformClick();
                    }
                    break;
                case "ADD NEW":
                    {
                        try
                        {
                            if (txbFirstName.Text == "" || txbLastName.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            Guest newGuest = new Guest() { FirstName = txbFirstName.Text, LastName = txbLastName.Text };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PostAsJsonAsync("/api/guests/", newGuest).Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        btnRefresh.PerformClick();

                    }
                    break;

            }

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/guests").Result;
            var data = response.Content.ReadAsAsync<IEnumerable<Guest>>().Result;
            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Successfully loaded all data!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Something went wrong", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dataGuestView.DataSource = data;
        }
    }
}
