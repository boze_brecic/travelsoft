﻿namespace Client
{
    partial class RoomsForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.AddNewToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.EditToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.DeleteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnShowAll = new System.Windows.Forms.Button();
            this.dataRoomsView = new System.Windows.Forms.DataGridView();
            this.BtnAction = new System.Windows.Forms.Button();
            this.lblAddress = new System.Windows.Forms.Label();
            this.txbId = new System.Windows.Forms.TextBox();
            this.lblId = new System.Windows.Forms.Label();
            this.lblAction = new System.Windows.Forms.Label();
            this.cmbAddress = new System.Windows.Forms.ComboBox();
            this.lblRoomNum = new System.Windows.Forms.Label();
            this.txbRoomNum = new System.Windows.Forms.TextBox();
            this.btnPopulateAdr = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataRoomsView)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AddNewToolStripMenuItem,
            this.EditToolStripMenuItem,
            this.DeleteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1127, 28);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // AddNewToolStripMenuItem
            // 
            this.AddNewToolStripMenuItem.Name = "AddNewToolStripMenuItem";
            this.AddNewToolStripMenuItem.Size = new System.Drawing.Size(83, 24);
            this.AddNewToolStripMenuItem.Text = "Add New";
            this.AddNewToolStripMenuItem.Click += new System.EventHandler(this.AddNewToolStripMenuItem_Click);
            // 
            // EditToolStripMenuItem
            // 
            this.EditToolStripMenuItem.Name = "EditToolStripMenuItem";
            this.EditToolStripMenuItem.Size = new System.Drawing.Size(47, 24);
            this.EditToolStripMenuItem.Text = "Edit";
            this.EditToolStripMenuItem.Click += new System.EventHandler(this.EditToolStripMenuItem_Click);
            // 
            // DeleteToolStripMenuItem
            // 
            this.DeleteToolStripMenuItem.Name = "DeleteToolStripMenuItem";
            this.DeleteToolStripMenuItem.Size = new System.Drawing.Size(65, 24);
            this.DeleteToolStripMenuItem.Text = "Delete";
            this.DeleteToolStripMenuItem.Click += new System.EventHandler(this.DeleteToolStripMenuItem_Click);
            // 
            // BtnShowAll
            // 
            this.BtnShowAll.Location = new System.Drawing.Point(192, 71);
            this.BtnShowAll.Name = "BtnShowAll";
            this.BtnShowAll.Size = new System.Drawing.Size(141, 49);
            this.BtnShowAll.TabIndex = 4;
            this.BtnShowAll.Text = "Show All";
            this.BtnShowAll.UseVisualStyleBackColor = true;
            this.BtnShowAll.Click += new System.EventHandler(this.BtnShowAll_Click);
            // 
            // dataRoomsView
            // 
            this.dataRoomsView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataRoomsView.Location = new System.Drawing.Point(12, 126);
            this.dataRoomsView.Name = "dataRoomsView";
            this.dataRoomsView.RowTemplate.Height = 24;
            this.dataRoomsView.Size = new System.Drawing.Size(539, 300);
            this.dataRoomsView.TabIndex = 3;
            // 
            // BtnAction
            // 
            this.BtnAction.Location = new System.Drawing.Point(767, 321);
            this.BtnAction.Name = "BtnAction";
            this.BtnAction.Size = new System.Drawing.Size(100, 51);
            this.BtnAction.TabIndex = 18;
            this.BtnAction.UseVisualStyleBackColor = true;
            this.BtnAction.Click += new System.EventHandler(this.BtnAction_Click);
            // 
            // lblAddress
            // 
            this.lblAddress.AutoSize = true;
            this.lblAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAddress.Location = new System.Drawing.Point(557, 183);
            this.lblAddress.Name = "lblAddress";
            this.lblAddress.Padding = new System.Windows.Forms.Padding(4);
            this.lblAddress.Size = new System.Drawing.Size(100, 33);
            this.lblAddress.TabIndex = 16;
            this.lblAddress.Text = "Address";
            // 
            // txbId
            // 
            this.txbId.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbId.Location = new System.Drawing.Point(719, 132);
            this.txbId.Name = "txbId";
            this.txbId.Size = new System.Drawing.Size(235, 26);
            this.txbId.TabIndex = 15;
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblId.Location = new System.Drawing.Point(557, 125);
            this.lblId.Name = "lblId";
            this.lblId.Padding = new System.Windows.Forms.Padding(4);
            this.lblId.Size = new System.Drawing.Size(38, 33);
            this.lblId.TabIndex = 14;
            this.lblId.Text = "Id";
            // 
            // lblAction
            // 
            this.lblAction.AutoSize = true;
            this.lblAction.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAction.Location = new System.Drawing.Point(762, 40);
            this.lblAction.Name = "lblAction";
            this.lblAction.Padding = new System.Windows.Forms.Padding(4);
            this.lblAction.Size = new System.Drawing.Size(8, 33);
            this.lblAction.TabIndex = 13;
            // 
            // cmbAddress
            // 
            this.cmbAddress.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbAddress.FormattingEnabled = true;
            this.cmbAddress.Location = new System.Drawing.Point(719, 188);
            this.cmbAddress.Name = "cmbAddress";
            this.cmbAddress.Size = new System.Drawing.Size(235, 28);
            this.cmbAddress.TabIndex = 19;
            this.cmbAddress.Text = "Select available address";
            // 
            // lblRoomNum
            // 
            this.lblRoomNum.AutoSize = true;
            this.lblRoomNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRoomNum.Location = new System.Drawing.Point(557, 246);
            this.lblRoomNum.Name = "lblRoomNum";
            this.lblRoomNum.Padding = new System.Windows.Forms.Padding(4);
            this.lblRoomNum.Size = new System.Drawing.Size(156, 33);
            this.lblRoomNum.TabIndex = 20;
            this.lblRoomNum.Text = "Room Number";
            // 
            // txbRoomNum
            // 
            this.txbRoomNum.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txbRoomNum.Location = new System.Drawing.Point(719, 247);
            this.txbRoomNum.Name = "txbRoomNum";
            this.txbRoomNum.Size = new System.Drawing.Size(235, 26);
            this.txbRoomNum.TabIndex = 21;
            // 
            // btnPopulateAdr
            // 
            this.btnPopulateAdr.Location = new System.Drawing.Point(990, 179);
            this.btnPopulateAdr.Name = "btnPopulateAdr";
            this.btnPopulateAdr.Size = new System.Drawing.Size(87, 47);
            this.btnPopulateAdr.TabIndex = 22;
            this.btnPopulateAdr.Text = "Populate Addresses";
            this.btnPopulateAdr.UseVisualStyleBackColor = true;
            this.btnPopulateAdr.Click += new System.EventHandler(this.btnPopulateAdr_Click);
            // 
            // RoomsForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1127, 482);
            this.Controls.Add(this.btnPopulateAdr);
            this.Controls.Add(this.txbRoomNum);
            this.Controls.Add(this.lblRoomNum);
            this.Controls.Add(this.cmbAddress);
            this.Controls.Add(this.BtnAction);
            this.Controls.Add(this.lblAddress);
            this.Controls.Add(this.txbId);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.lblAction);
            this.Controls.Add(this.BtnShowAll);
            this.Controls.Add(this.dataRoomsView);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "RoomsForm";
            this.Text = "RoomsForm";
            this.Load += new System.EventHandler(this.RoomsForm_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataRoomsView)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem AddNewToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem EditToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem DeleteToolStripMenuItem;
        private System.Windows.Forms.Button BtnShowAll;
        private System.Windows.Forms.DataGridView dataRoomsView;
        private System.Windows.Forms.Button BtnAction;
        private System.Windows.Forms.Label lblAddress;
        private System.Windows.Forms.TextBox txbId;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.Label lblAction;
        private System.Windows.Forms.ComboBox cmbAddress;
        private System.Windows.Forms.Label lblRoomNum;
        private System.Windows.Forms.TextBox txbRoomNum;
        private System.Windows.Forms.Button btnPopulateAdr;
    }
}