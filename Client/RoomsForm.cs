﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Client
{
    public partial class RoomsForm : Form
    {
        public RoomsForm()
        {
            InitializeComponent();
        }
        public void setAll(bool flag)
        {
            txbId.Enabled = flag;
            cmbAddress.Enabled = flag;
            BtnAction.Enabled = flag;
            btnPopulateAdr.Enabled = flag;
            txbRoomNum.Enabled = flag;
        }

        public void UI(string action)
        {
            switch (action)
            {
                case "EDIT":
                    {
                        setAll(true);
                        BtnAction.Text = action;
                    }
                    break;
                case "ADD NEW":
                    {
                        setAll(true);
                        txbId.Enabled = false;
                        BtnAction.Text = action;
                    }
                    break;
                case "DELETE":
                    {
                        setAll(false);
                        txbId.Enabled = true;
                        BtnAction.Enabled = true;
                        BtnAction.Text = action;
                    }
                    break;
            }
        }

        private void BtnShowAll_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/rooms").Result;
            var data = response.Content.ReadAsAsync<IEnumerable<Room>>().Result;
            if (response.IsSuccessStatusCode)
            {
                MessageBox.Show("Successfully loaded all data!", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
                MessageBox.Show("Something went wrong", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            dataRoomsView.DataSource = data;
        }

        private void RoomsForm_Load(object sender, EventArgs e)
        {
            setAll(false);
        }

        private void AddNewToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("ADD NEW");
        }

        private void EditToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("EDIT");
        }

        private void DeleteToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UI("DELETE");
        }

        private void BtnAction_Click(object sender, EventArgs e)
        {
            switch (BtnAction.Text)
            {
                case "EDIT":
                    {
                        try
                        {
                            if (txbId.Text == "" || cmbAddress.Text == "Select available address" || txbRoomNum.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            Room newRoom = new Room() { Id = int.Parse(txbId.Text), Address = cmbAddress.Text, RoomNumber = int.Parse(txbRoomNum.Text) };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PutAsJsonAsync($"/api/rooms/{int.Parse(txbId.Text)}", newRoom).Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "DELETE":
                    {
                        try
                        {
                            if (txbId.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.DeleteAsync($"/api/rooms/{int.Parse(txbId.Text)}").Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;
                case "ADD NEW":
                    {
                        try
                        {
                            if (cmbAddress.Text == "Select available address" || txbRoomNum.Text == "")
                                throw new Exception("You have not filled in some fields!");
                            Room newRoom = new Room() { Address = cmbAddress.Text, RoomNumber = int.Parse(txbRoomNum.Text) };
                            HttpClient client = new HttpClient();
                            client.BaseAddress = new Uri("http://localhost:59942/");
                            HttpResponseMessage response = client.PostAsJsonAsync("/api/rooms/", newRoom).Result;
                        }
                        catch (Exception exc)
                        {
                            MessageBox.Show(exc.ToString(), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            break;
                        }
                        BtnShowAll.PerformClick();
                    }
                    break;

            }
        }

        private void btnPopulateAdr_Click(object sender, EventArgs e)
        {
            HttpClient client = new HttpClient();
            client.BaseAddress = new Uri("http://localhost:59942/");
            HttpResponseMessage response = client.GetAsync("api/buildings").Result;
            var dataAddress = response.Content.ReadAsAsync<IEnumerable<Building>>().Result;
            for (int i = 0; i < dataAddress.Count(); i++)
            {
                cmbAddress.Items.Add(dataAddress.ElementAt(i).Address.ToString());
            }
        }
    }
}
