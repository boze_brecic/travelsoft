﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RentWebApi.Models;

namespace RentWebApi.Controllers
{
    public class BookingsController : ApiController
    {
        private RentWebApiContext db = new RentWebApiContext();

        // GET: api/Bookings
        public IQueryable<BookingDto> GetBookings()
        {
            var bookingDto = from b in db.Bookings
                             select new BookingDto()
                             {
                                 Id=b.Id,
                                 FirstName = b.Guest.FirstName,
                                 LastName = b.Guest.LastName,
                                 BuildingAddress = b.Room.Building.Address,
                                 RoomNumber = b.Room.RoomNum,
                                 CheckIn = b.CheckIn,
                                 CheckOut = b.CheckOut
                             };
            return bookingDto;
        }

        // GET: api/Bookings/5
        [ResponseType(typeof(Booking))]
        public async Task<IHttpActionResult> GetBooking(int id)
        {
            Booking booking = await db.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }

            return Ok(booking);
        }

        // PUT: api/Bookings/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutBooking(int id, BookingDto booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != booking.Id)
            {
                return BadRequest();
            }
            Booking newBooking = new Booking()
            {
                Id = id,
                GuestId = db.Guests.SingleOrDefault(x => x.FirstName == booking.FirstName).Id,
                RoomId = db.Rooms.SingleOrDefault(x => x.RoomNum == booking.RoomNumber).Id,
                CheckIn=booking.CheckIn,
                CheckOut=booking.CheckOut
                
            };

            db.Entry(newBooking).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookingExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Bookings
        [ResponseType(typeof(BookingDto))]
        public async Task<IHttpActionResult> PostBooking(BookingDto booking)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            Booking newBooking = new Booking()
            {
                Id = booking.Id,
                GuestId = db.Guests.SingleOrDefault(x => x.FirstName == booking.FirstName).Id,
                RoomId = db.Rooms.SingleOrDefault(x => x.RoomNum == booking.RoomNumber).Id,
                CheckIn = booking.CheckIn,
                CheckOut = booking.CheckOut
            };
            db.Bookings.Add(newBooking);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = booking.Id }, newBooking);
        }

        // DELETE: api/Bookings/5
        [ResponseType(typeof(Booking))]
        public async Task<IHttpActionResult> DeleteBooking(int id)
        {
            Booking booking = await db.Bookings.FindAsync(id);
            if (booking == null)
            {
                return NotFound();
            }

            db.Bookings.Remove(booking);
            await db.SaveChangesAsync();

            return Ok(booking);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool BookingExists(int id)
        {
            return db.Bookings.Count(e => e.Id == id) > 0;
        }
    }
}