﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using RentWebApi.Models;

namespace RentWebApi.Controllers
{
    public class RoomsController : ApiController
    {
        private RentWebApiContext db = new RentWebApiContext();

        // GET: api/Rooms
        public IQueryable<RoomDto> GetRooms()
        {
            var roomDto = from r in db.Rooms
                          select new RoomDto()
                          {
                              Id = r.Id,
                              Address = r.Building.Address,
                              RoomNumber = r.RoomNum
                          };
            return roomDto;
        }

        // GET: api/Rooms/5
        [ResponseType(typeof(Room))]
        public async Task<IHttpActionResult> GetRoom(int id)
        {
            Room room = await db.Rooms.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            return Ok(room);
        }

        // PUT: api/Rooms/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutRoom(int id, RoomDto room)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != room.Id)
            {
                return BadRequest();
            }
            Room newRoom = new Room()
            {
                Id = id,
                BuildingId = db.Buildings.SingleOrDefault(x => x.Address == room.Address).Id,
                RoomNum = room.RoomNumber

            };
            db.Entry(newRoom).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!RoomExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Rooms
        [ResponseType(typeof(RoomDto))]
        public async Task<IHttpActionResult> PostRoom(RoomDto room)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Room newRoom = new Room()
            {
                Id = room.Id,
                BuildingId = db.Buildings.SingleOrDefault(x => x.Address == room.Address).Id,
                RoomNum = room.RoomNumber                

            };
            db.Rooms.Add(newRoom);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = room.Id }, newRoom);
        }

        // DELETE: api/Rooms/5
        [ResponseType(typeof(Room))]
        public async Task<IHttpActionResult> DeleteRoom(int id)
        {
            Room room = await db.Rooms.FindAsync(id);
            if (room == null)
            {
                return NotFound();
            }

            db.Rooms.Remove(room);
            await db.SaveChangesAsync();

            return Ok(room);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool RoomExists(int id)
        {
            return db.Rooms.Count(e => e.Id == id) > 0;
        }
    }
}