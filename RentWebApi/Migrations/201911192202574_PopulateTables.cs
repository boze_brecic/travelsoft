namespace RentWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTables : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Guests(FirstName,LastName) VALUES('Ivan','Ivanic')");
            Sql("INSERT INTO Guests(FirstName,LastName) VALUES('Ante','Antic')");
            Sql("INSERT INTO Guests(FirstName,LastName) VALUES('Mate','Matic')");
            Sql("INSERT INTO Buildings(Address) VALUES('Poljicka ulica 21')");
            Sql("INSERT INTO Buildings(Address) VALUES('Vukovarska 15')");
            Sql("INSERT INTO Buildings(Address) VALUES('Osjecka 3')");

        }
        
        public override void Down()
        {
        }
    }
}
