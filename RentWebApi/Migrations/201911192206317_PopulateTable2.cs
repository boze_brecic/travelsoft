namespace RentWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTable2 : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Rooms(RoomNum,BuildingId) VALUES(17,7)");
            Sql("INSERT INTO Rooms(RoomNum,BuildingId) VALUES(18,7)");
            Sql("INSERT INTO Rooms(RoomNum,BuildingId) VALUES(101,8)");
            Sql("INSERT INTO Rooms(RoomNum,BuildingId) VALUES(102,8)");
            Sql("INSERT INTO Rooms(RoomNum,BuildingId) VALUES(10,9)");

        }
        
        public override void Down()
        {
        }
    }
}
