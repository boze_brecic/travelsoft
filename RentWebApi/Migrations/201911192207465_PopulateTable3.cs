namespace RentWebApi.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class PopulateTable3 : DbMigration
    {
        public override void Up()
        {
            Sql("INSERT INTO Bookings(CheckIn,CheckOut,GuestId,RoomId) VALUES(2019-11-19,2019-11-21,7,2)");
            Sql("INSERT INTO Bookings(CheckIn,CheckOut,GuestId,RoomId) VALUES(2019-11-19,2019-11-23,7,3)");
            Sql("INSERT INTO Bookings(CheckIn,CheckOut,GuestId,RoomId) VALUES(2019-11-19,2019-11-27,8,4)");
            Sql("INSERT INTO Bookings(CheckIn,CheckOut,GuestId,RoomId) VALUES(2019-11-19,2019-12-01,9,5)");
        }
        
        public override void Down()
        {
        }
    }
}
