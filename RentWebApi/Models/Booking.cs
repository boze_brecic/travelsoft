﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class Booking
    {
        public int Id { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
        public Room Room { get; set; }
        public int RoomId { get; set; }
        public Guest Guest { get; set; }
        public int GuestId { get; set; }
    }
}