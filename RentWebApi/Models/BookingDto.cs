﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class BookingDto
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string BuildingAddress { get; set; }
        public int RoomNumber { get; set; }
        public DateTime CheckIn { get; set; }
        public DateTime CheckOut { get; set; }
    }
}