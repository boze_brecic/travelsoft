﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class Guest
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public ICollection<Booking> Booking { get; set; }
    }
}