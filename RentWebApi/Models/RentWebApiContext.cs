﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class RentWebApiContext : DbContext
    {
        // You can add custom code to this file. Changes will not be overwritten.
        // 
        // If you want Entity Framework to drop and regenerate your database
        // automatically whenever you change your model schema, please use data migrations.
        // For more information refer to the documentation:
        // http://msdn.microsoft.com/en-us/data/jj591621.aspx
    
        public RentWebApiContext() : base("name=RentWebApiContext")
        {
        }

        public System.Data.Entity.DbSet<RentWebApi.Models.Booking> Bookings { get; set; }

        public System.Data.Entity.DbSet<RentWebApi.Models.Guest> Guests { get; set; }

        public System.Data.Entity.DbSet<RentWebApi.Models.Room> Rooms { get; set; }

        public System.Data.Entity.DbSet<RentWebApi.Models.Building> Buildings { get; set; }
    }
}
