﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class Room
    {
        public int Id { get; set; }
        public int RoomNum { get; set; }
        //FK
        public int BuildingId { get; set; }
        public Building Building { get; set; }
        
        public ICollection<Booking> Booking { get; set; }
    }
}