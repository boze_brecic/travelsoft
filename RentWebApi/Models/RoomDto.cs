﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace RentWebApi.Models
{
    public class RoomDto
    {
        public int Id { get; set; }
        public string Address { get; set; }
        public int RoomNumber { get; set; }
    }
}